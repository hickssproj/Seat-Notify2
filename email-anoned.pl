use strict;
use warnings;
use Term::ReadKey;
use Emailesque;

# change these parameters to fit your situation:
my $debug = 0; # 0 for no debugging output, 1 for debugging output
my $from = '';    # from line of text message
#my $to   = ''; # to line of text message
my $to   = '';    # for intial testing without SMS
my $smtp = '';       # for gmail
my $login = '';    # for gmail
my $password = '';                 # should be read from stdin

# Get the password from the user for the SMTP server
$password = &getPassword();

# Create a new email with the following attributes


# Asks the user for a password, echoes with the "*" character as the user types
# Returns: Password entered from the user
sub getPassword {
  ReadMode('noecho');
  ReadMode('raw');
  my $pass = '';

  # Flush the buffer so other programs can't read what was typed
  $| = 1;

  print "Enter password for $login on $smtp: ";
  while (1) {
    my $c;
    1 until defined($c = ReadKey(-1));
    last if ($c eq "\n" || $c eq "\r");
    print "*";
    $pass .= $c;
  }

  ReadMode('restore');
  print "\n[$pass]\n" if ($debug);
  return $pass;
}