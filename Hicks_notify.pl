#Darryl Hicks
#SP16 ICS 215 - Perl Advanced Assignment
#Pulls a web page, feeds it into the Seats.pl, returns seats if available, sends notification to email or phone
#Errors/Bugs: Adds extra '*' characters when attempting to backspace when entering password in console.

use strict;
use warnings;
use Term::ReadKey;
use Emailesque;
use LWP::Simple;

#Defines input arguments, defines other global variables
my ($ClassNumber, $SendTo, $Carrier ) = @ARGV;
my $argSize = @ARGV;
getstore('https://www.sis.hawaii.edu/uhdad/avail.classes?i=LEE&t=201630&s=ICS','retrieved_contents.html');
my $EmailMessage;

# Code from email.pl (ver. [2/10/16], by Ed Meyer):
my $debug = 0; # 0 for no debugging output, 1 for debugging output
my $from = 'ics215.lcc@gmail.com';    # from line of text message
my $smtp = 'smtp.googlemail.com';       # for gmail
my $login = 'ics215.lcc@gmail.com';    # for gmail
my $password = '';                 # should be read from stdin

# Get the password from the user for the SMTP server
$password = &getPassword();

# Returns: Password entered from the user
sub getPassword {
# Asks the user for a password, echoes with the "*" character as the user types
  ReadMode('noecho');
  ReadMode('raw');
  my $pass = '';

  # Flush the buffer so other programs can't read what was typed
  $| = 1;

  print "Enter password for $login on $smtp: ";
  while (1) {
    my $c;
    1 until defined($c = ReadKey(-1));
    last if ($c eq "\n" || $c eq "\r");
    print "*";
    $pass .= $c;
  }

  ReadMode('restore');
  print "\n[$pass]\n" if ($debug);
  return $pass;
}

#Hash of popular cellphone carriers
my %CellNumbers = (
	att => '@txt.att.net',
	spr => '@messaging.sprintpcs.com',
	tmo => '@tmomail.net',
	ver => '@vtext.com',
	vir => '@vmobl.com'
	
);


##Program should take 2 or 3 arguments (Course Number, Text Message, [carrier of cellphone])
#If arguments not met should print error

#If 2 args, assume second arg is email
if ($argSize == 2){
	chomp($SendTo);
	#use regex to check against entered email
	if ($SendTo =~ m/(.+)@(.+)\.(\w+)/){
		continue
	}
	#If it doesn't match, exit program with error message
	else {print "Invalid email!\n"}
	
	
}

#If 3 args, assume second arg is phone number and third arg is carrier
elsif ($argSize == 3){
	
	#Removes any possible "\n" characters
	chomp($SendTo,$Carrier);
	
	#Checks input phone number length
	if (length($SendTo) == 10){
		
		#Clunky way of verifying cellphone carrier validity 
		my $CellCheck = "0";
		for (keys %CellNumbers){
			if ($_ eq $Carrier){
				$SendTo = "$SendTo" . "$CellNumbers{$_}";
				$CellCheck = "1"
			}
		}
		#If third arg isn't valid, exit 
		die "That code wasn't a recognized extension! Please enter the correct 3 character extension for your carrier\n\tex: tmo = T-Mobile, vir = Virgin, etc\n" if ($CellCheck == "0");
					
	}
	
	#If second arg isn't 10 digits, exit with error message
	else {die "Invalid Phone Number!\n"}
	
}

else{
	die "Something terrible has happened. Please check your inputs and try again.\n"
};

&CheckCourses;

#Checks course availablity: Takes defined class number and sends the appropriate email
sub CheckCourses{
	#Defines the default variable because perl spits back warnings if it isn't defined 
	$_ = '';
	
	#Debugging warning; useful if unsure if terminal/perl is frozen
	print "\n[Code is currently in a loop, if you need to end it, hit CTRL+C]\n";
	
	#Continues to run until the default variable is a number
	until ($_ =~ m/\d+/){
		
		#Captures the output of the seats.pl - either a string or an integer
		$_ = `perl Hicks_seats.pl $ClassNumber retrieved_contents.html`;
		
		#Checks to see if the variable is an integer, if so, exit the loop and continue the subroutine
		last if($_ =~ m/\d+/);
		
		#Emails the string from seats.pl and puts the loop to sleep for 5 minutes
		$EmailMessage = "$_ Retrying in 5 minutes...\n";
		&PrepEmail;
		sleep(300);
		
		#Grabs the newest version of retrieved_contents.html
		getstore('https://www.sis.hawaii.edu/uhdad/avail.classes?i=LEE&t=201630&s=ICS','retrieved_contents.html');
		
	}
	#Sends the "Seats are available" email and exits the subroutine
	$EmailMessage = "There are $_ seats available for class number $ClassNumber\n";
	&PrepEmail
}

#Prepares and sends the email 
sub PrepEmail{
	my $email = Emailesque->new(
	to      => $SendTo,
	from    => $from,
	subject => "Seat Availability for Class $ClassNumber",
	message => $EmailMessage,
	ssl     => 1,
	driver  => 'smtp',
	host    => $smtp,
	port    => 465,
	user    => $login,
	pass	=> $password,
	debug   => $debug
);

	# Send the message.
	$email->send;

}